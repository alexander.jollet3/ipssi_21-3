
## Title
Short and concise name of the merge request

## Description
A few sentences describing the overall goals of the pull request's commits.

## Todos
- [ ] Tests
- [ ] Documentation

## Bug Fix  
- [ ] YES
- [ ] NO

If "YES" please enter ticket number : Fix#0000

## Steps to Test or Reproduce
Outline the steps to test or reproduce the PR here.

```
git clone ...
git checkout <feature_branch>
```

## Impacted Areas in Application
List general components of the application that this PR will affect:

-
-

* 